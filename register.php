<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Local plugin "Self cohort membership"
 *
 * @package   local_selfcohort
 * @copyright 2020 Daniel Neis Araujo <daniel@adapta.online>
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
require_once('../../config.php');
require_once($CFG->dirroot . '/cohort/lib.php');

$url = new moodle_url('/local/selfcohort/register.php');
$PAGE->set_context(context_system::instance());
$PAGE->set_url($url);
$PAGE->set_title($SITE->fullname);
$PAGE->set_heading($SITE->fullname);

require_login();

$cohorts = $DB->get_records('cohort', ['component' => 'local_selfcohort']);

if (empty($cohorts)) {
    $memberships = [];
    $cohortsids = [];
} else {
    $cohortsids = array_map(function($c) {
        return (int)$c->id;
    }, $cohorts);
    list($cohortssql, $params) = $DB->get_in_or_equal($cohortsids, SQL_PARAMS_NAMED);
    $sql = 'SELECT cohortid FROM {cohort_members} WHERE userid = :userid and cohortid '. $cohortssql;
    $params['userid'] = $USER->id;
    $memberships = $DB->get_records_sql($sql, $params);
}

$form = new \local_selfcohort\register_form(null, ['cohorts' => $cohorts, 'memberships' => $memberships]);

if ($form->is_cancelled()) {
    redirect($url);
} else if ($data = $form->get_data()) {
    foreach ($cohorts as $c) {
        cohort_remove_member($c->id, $USER->id);
    }
    if (!isset($data->cohort)) {
        $data->cohort = [];
    }
    if (!is_array($data->cohort)) {
        $data->cohort = [$data->cohort => 1];
    }
    foreach ($data->cohort as $cid => $value) {
        if (($value == 1) && in_array($cid, $cohortsids)) {
            cohort_add_member($cid, $USER->id);
        }
    }
    redirect($url);
}

echo $OUTPUT->header(),
     $form->display(),
     $OUTPUT->footer();
