<?php
// This file is part of Moodle - https://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <https://www.gnu.org/licenses/>.

/**
 * This adds the custom fields management page.
 *
 * @package     local_selfcohort
 * @copyright   2020 Daniel Neis Araujo <daniel@adapta.online>
 * @license     https://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die;

if ($hassiteconfig) { // Needs this condition or there is error on login page.
    $ADMIN->add('accounts',
        new admin_externalpage('local_selfcohort_config', new lang_string('pluginname', 'local_selfcohort'),
            new moodle_url('/local/selfcohort/cohorts.php'), ['moodle/cohort:manage']
        )
    );

    $settings = new admin_settingpage('local_selfcohort', new lang_string('pluginname', 'local_selfcohort'));
    $settings->add(new admin_setting_configcheckbox('local_selfcohort/selectmany',
        new lang_string('selectmany', 'local_selfcohort'), new lang_string('selectmanydesc', 'local_selfcohort'), '1'
        )
    );

    //$settings->add($selectmany);
    $ADMIN->add('localplugins', $settings);
}
