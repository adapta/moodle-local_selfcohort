<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Local plugin "Profile field based cohort membership" - Main class for matching up users with themes
 *
 * @package   local_selfcohort
 * @copyright 2016 Davo Smith, Synergy Learning UK on behalf of Alexander Bias, Ulm University <alexander.bias@uni-ulm.de>
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace local_selfcohort;

use html_writer;

defined('MOODLE_INTERNAL') || die();

/**
 * Class selfcohort
 * @package local_selfcohort
 * @copyright 2016 Davo Smith, Synergy Learning UK on behalf of Alexander Bias, Ulm University <alexander.bias@uni-ulm.de>
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class selfcohort  {
    /** @var string the database table to use */
    protected static $tablename = 'local_selfcohort';

    /** @var string[] the list of available actions */
    protected static $actions = ['cohorts', 'members'];

    /** @var string[] the list of cohorts id => displayname */
    protected $possiblevalues = null;

    /**
     * constructor
     */
    public function __construct() {
        global $PAGE;

        if (!static::$tablename) {
            throw new \coding_exception('Must set $tablename in derived classes');
        }
        $this->action = optional_param('action', null, PARAM_ALPHA);
        if (!in_array($this->action, static::$actions)) {
            $this->action = 'cohorts';
        }
        if (!PHPUNIT_TEST && !CLI_SCRIPT) {
            $url = new \moodle_url($PAGE->url, ['action' => $this->action]);
            $PAGE->set_url($url);
        }
    }


    /**
     * Get the URL of the main page for this plugin.
     * @return \moodle_url
     */
    protected function get_index_url() {
        return new \moodle_url('/local/selfcohort/cohorts.php');
    }

    /**
     * Get a list of possible values that fields can be mapped onto.
     * @return string[] id => displayname
     */
    protected function get_possible_values() {
        if ($this->possiblevalues === null) {
            $this->possiblevalues = static::load_possible_values();
        }
        return $this->possiblevalues;
    }

    /**
     * Output the complete form for editing profile field mapping rules.
     * @return string
     */
    public function output_form() {
        global $OUTPUT;

        $out = '';

        $tabs = $this->get_tabs();
        $out .= $OUTPUT->render($tabs);

        if ($this->action == 'members') {
            $out .= $this->output_members();
        } else {
            $out .= $this->form->render();
        }

        return $out;
    }

    /**
     * Allow subclasses to define extra tabs to be included at the top of the page.
     * @return \tabobject[]
     */
    protected function get_tabs() {
        $tabs = [
            new \tabobject('members', new \moodle_url('/local/selfcohort/cohorts.php', ['action' => 'members']),
                           get_string('members', 'local_selfcohort')),
            new \tabobject('cohorts', new \moodle_url('/local/selfcohort/cohorts.php'),
                           get_string('selectcohorts', 'local_selfcohort'))
        ];
        $tabtree = new \tabtree($tabs, $this->action);

        return $tabtree;
    }

    /**
     * Output the cohort members list.
     * @return string
     */
    protected function output_members() {
        global $OUTPUT, $DB;

        $out = \html_writer::tag('div', get_string('membersintro', 'local_selfcohort').'<br/>'.
                                 get_string('invisiblecohortsnote', 'local_selfcohort'),
                                 array('id' => 'intro', 'class' => 'box generalbox'));

        $namefields = \core_user\fields::get_name_fields();
        $namefields = implode(',', $namefields);
        $sql = "
          SELECT c.id AS cohortid, c.name AS cohortname, u.id, {$namefields}
            FROM {cohort} c
            LEFT JOIN {cohort_members} cm ON cm.cohortid = c.id
            LEFT JOIN {user} u ON u.id = cm.userid
           WHERE c.component = 'local_selfcohort'
           ORDER BY c.name, c.id, u.lastname, u.firstname
        ";
        $users = $DB->get_recordset_sql($sql);

        $lastcohortid = null;
        $lastcohortname = null;
        $list = '';
        $cohortmembers = [];
        foreach ($users as $user) {
            if ($user->cohortid != $lastcohortid) {
                if ($lastcohortid) {
                    $list .= $this->output_members_entry($lastcohortname, $cohortmembers);
                }
                $cohortmembers = [];
                $lastcohortid = $user->cohortid;
                $lastcohortname = $user->cohortname;
            }
            if ($user->id) {
                $userurl = new \moodle_url('/user/view.php', ['id' => $user->id]);
                $cohortmembers[] = html_writer::link($userurl, fullname($user));
            }
        }
        if ($lastcohortid) {
            $list .= $this->output_members_entry($lastcohortname, $cohortmembers);
        }

        $out .= html_writer::div($list, '', ['id' => 'profilecohort-cohortlist', 'role' => 'tablist',
                                             'aria-multiselectable' => 'true']);

        return $out;
    }

    /**
     * Render a cohortlist entry for output_members().
     * @param string $cohortname
     * @param string[] $cohortmembers
     * @return string
     */
    private function output_members_entry($cohortname, $cohortmembers) {
        $out = '';

        // Create HTML element ID from cohortname.
        $id = 'profilecohort-cohortlist-'.preg_replace('/\W+/', '', strtolower($cohortname));

        // Bootstrap collapse header.
        $out .= html_writer::start_div('card-header', ['id' => $id.'-heading', 'role' => 'tab']);
        $out .= html_writer::link('#'.$id, format_string($cohortname),
                ['class' => 'collapsed', 'data-toggle' => 'collapse', 'data-parent' => '#profilecohort-cohortlist',
                 'aria-expanded' => 'false', 'aria-controls' => $id]);
        $out .= html_writer::end_div();

        // Bootstrap collapse content.
        if ($cohortmembers) {
            $content = '';
            foreach ($cohortmembers as $cohortmember) {
                $content .= html_writer::tag('li', $cohortmember);
            }
            $content = html_writer::tag('ul', $content);
        } else {
            $content = get_string('nousers', 'local_selfcohort');
        }
        $out .= html_writer::start_div('collapse', ['id' => $id, 'role' => 'tabpanel', 'aria-labelledby' => $id.'-heading']);
        $out .= html_writer::div($content, 'card-body');
        $out .= html_writer::end_div();

        return html_writer::div($out, 'card');
    }

    /**
     * Load a list of possible values that fields can be mapped onto.
     * @return string[] id => displayname
     */
    protected static function load_possible_values() {
        global $DB;
        $cohorts = $DB->get_records_menu('cohort', ['component' => 'local_selfcohort'], 'name', 'id, name');
        return $cohorts;
    }

    /**
     * Process the form for editing which cohorts should be managed by this plugin.
     */
    public function process_form() {
        global $DB;

        $select = "(component = '' OR component = 'local_selfcohort')";
        $allcohorts = $DB->get_records_select('cohort', $select, [], 'name', 'id, name, component');

        $custom = ['cohorts' => $allcohorts];
        $this->form = new cohort_form(null, $custom);

        $redir = new \moodle_url('/local/selfcohort/cohorts.php');
        if ($this->form->is_cancelled()) {
            redirect($redir);
        }
        if ($formdata = $this->form->get_data()) {
            $changed = false;
            foreach ($allcohorts as $cohort) {
                if ($formdata->cohort[$cohort->id]) {
                    if ($cohort->component != 'local_selfcohort') {
                        // Cohort selected - start managing this cohort.
                        $DB->set_field('cohort', 'component', 'local_selfcohort', ['id' => $cohort->id]);
                        $changed = true;
                    }
                } else {
                    if ($cohort->component == 'local_selfcohort') {
                        // Cohort deselected - stop managing this cohort.
                        $DB->set_field('cohort', 'component', '', ['id' => $cohort->id]);
                        $changed = true;
                    }
                }
            }
            redirect($redir);
        }
    }
}
