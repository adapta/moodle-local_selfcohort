<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Local plugin "selfcohort" form to register to cohorts
 *
 * @package   local_selfcohort
 * @copyright 2020 Daniel Neis Araujo <daniel@adapta.online>
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace local_selfcohort;

defined('MOODLE_INTERNAL') || die();
require_once($CFG->libdir.'/formslib.php');

/**
 * Class register_form
 * @package   local_selfcohort
 * @copyright 2020 Daniel Neis Araujo <daniel@adapta.online>
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class register_form extends \moodleform {

    /**
     * Form definition.
     */
    protected function definition() {
        $mform = $this->_form;
        $cohorts = $this->_customdata['cohorts'];
        $memberships = $this->_customdata['memberships'] ?? [];
        $selecmany = \get_config('local_selfcohort', 'selectmany');
        $mform->addElement('html', \html_writer::tag('div', get_string('registerintro', 'local_selfcohort'),
                                                     array('id' => 'intro', 'class' => 'box generalbox')));

        if (!$cohorts) {
            $mform->addElement('html', \html_writer::tag('div', get_string('nocohorts', 'local_selfcohort'),
                                                         array('class' => 'alert alert-warning')));
        } else {
            if ($selecmany) {
                foreach ($cohorts as $cohort) {
                    $mform->addElement('advcheckbox', "cohort[$cohort->id]", null, format_string($cohort->name));
                    $mform->setDefault("cohort[$cohort->id]", isset($memberships[$cohort->id]));
                }
            } else {
                $cohorts_array = array('' => get_string('selectacohort', 'local_selfcohort'));
                foreach ($cohorts as $cohort) {
                    $cohorts_array[$cohort->id] = format_string($cohort->name);
                }
                $mform->addElement('select', 'cohort', get_string('cohort', 'local_selfcohort'), $cohorts_array);
                $mform->setType('cohort', PARAM_INT);
                $mform->addRule('cohort', get_string('required'), 'required');
                if ($cid = array_pop($memberships)) {
                    $mform->setDefault('cohort',$cid->cohortid);
                }
            }
        }

        $this->add_action_buttons();
    }
}
